import com.waghetti.fragile_white_box_test_example.characters.JCharacter;
import com.waghetti.fragile_white_box_test_example.characters.JCharacterFactory;
import com.waghetti.fragile_white_box_test_example.characters.JDigit;
import com.waghetti.fragile_white_box_test_example.sequence.JCharacterSequence;
import com.waghetti.fragile_white_box_test_example.sequence.JDigitSequence;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class DigitSequenceTest {

  private static JCharacterSequence digitSequence;

  @BeforeAll
  static void setupClass() {
    JCharacterFactory factory = JCharacterFactory.getInstance();
    digitSequence = new JDigitSequence();
    digitSequence.add(factory.create('a'));
    digitSequence.add(factory.create('1'));
    digitSequence.add(factory.create('b'));
  }

  @Test
  void testWhiteBox() {
    for (JCharacter character : digitSequence.getCharacterSequence()) {
      Assertions.assertTrue(character instanceof JDigit);
    }
  }

  @Test
  void testBlackBox() {
    Assertions.assertEquals("1", digitSequence.getAsString());
  }

}
