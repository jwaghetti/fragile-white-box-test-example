package com.waghetti.fragile_white_box_test_example.sequence;

import com.waghetti.fragile_white_box_test_example.characters.JCharacter;

public class JDigitSequence extends JCharacterSequence {

  @Override
  public void add(JCharacter character) {

    // Just ignoring if not a digit
    if (Character.toString(character.getValue()).matches("[0-9]")) {
      super.add(character);
    }

  }

}
