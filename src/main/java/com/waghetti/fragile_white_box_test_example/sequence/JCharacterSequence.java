package com.waghetti.fragile_white_box_test_example.sequence;

import com.waghetti.fragile_white_box_test_example.characters.JCharacter;

import java.util.LinkedList;

public class JCharacterSequence {

  private LinkedList<JCharacter> sequence = new LinkedList<>();

  public LinkedList<JCharacter> getCharacterSequence() {
    // Bad idea to expose a collection like that.
    // You can protect this list, of course. But
    // it is another issue when exposing what
    // shouldn't be exposed.
    return sequence;
  }

  public String getAsString() {
    StringBuffer result = new StringBuffer();
    for (JCharacter character : sequence) {
      result.append(character.getValue());
    }
    return result.toString();
  }

  public void add(JCharacter character) {
    sequence.add(character);
  }

}
