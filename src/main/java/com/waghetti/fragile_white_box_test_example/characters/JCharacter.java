package com.waghetti.fragile_white_box_test_example.characters;

public interface JCharacter {
  char getValue();
}
