package com.waghetti.fragile_white_box_test_example.characters;

public class JCharacterFactory {

  private static JCharacterFactory instance;

  private JCharacterFactory() {

  }

  public static JCharacterFactory getInstance() {
    if (instance == null) {
      instance = new JCharacterFactory();
    }
    return instance;
  }

  public JCharacter create(char character) {

    JCharacter result;

    if (java.lang.Character.toString(character).matches("[0-9]")) {
      result = new JDigit(character);
    } else {
      result = new JCharacterImpl(character);
    }

    return result;

  }

}
