package com.waghetti.fragile_white_box_test_example.characters;

public class JCharacterImpl implements JCharacter {

  private char character;

  protected JCharacterImpl(char character) {
    this.character = character;
  }

  public char getValue() {
    return character;
  }

}
